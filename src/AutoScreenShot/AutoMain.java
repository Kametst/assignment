package AutoScreenShot;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class AutoMain {
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        EventFiringWebDriver driver = new EventFiringWebDriver(new ChromeDriver());
        AutoScreenshot l = new AutoScreenshot();
        driver.register(l);
        driver.get("https://www.geeksforgeeks.org/");
        WebElement e = driver.findElement(By.linkText("Data Structures"));
        e.click();
    }
}
