package SelGrid;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.MalformedURLException;
import java.net.URL;

public class Selgrid {
    public static void main(String[] a) throws MalformedURLException {
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        //cap.setCapability("browserName", "chrome");
        cap.setPlatform(Platform.MAC);
        URL urlHub = new URL("http://192.168.29.55:4444/wd/hub/");
        WebDriver driver = new RemoteWebDriver( urlHub, cap);
        driver.get("https://www.w3schools.com/");
        System.out.print(driver.getTitle());
        driver.quit();
    }
}
